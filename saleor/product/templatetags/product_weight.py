import logging
import re
import warnings

from django import template
from django.conf import settings
from django.templatetags.static import static

logger = logging.getLogger(__name__)
register = template.Library()

@register.simple_tag()
def weight_in_grams(value):
    return str(value).replace('\..*', '')